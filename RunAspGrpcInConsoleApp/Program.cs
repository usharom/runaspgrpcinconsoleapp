﻿using System;
using System.Net;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Hosting;

namespace RunAspGrpcInConsoleApp
{
    class Program
    {
        private const int SevicePort = 5003;

        static void Main(string[] args)
        {
            var host = CreateHostBuilder(null).Build();
            var thread = new Thread(() => host.Run())
            {
                Name = "WebAppThread",
                IsBackground = true
            };
            thread.Start();
            Console.ReadKey();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(options =>
                        options.Listen(IPAddress.Any,
                            SevicePort, listenOptions =>
                            {
                                listenOptions.Protocols = HttpProtocols.Http2;
                            }));
                    webBuilder.UseStartup<Startup>();
                });
    }
}
